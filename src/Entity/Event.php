<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $startingAt;

    #[ORM\Column(type: 'datetime')]
    private \DateTime $endingAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function getStartingAt(): \DateTime
    {
        return $this->startingAt;
    }

    public function setStartingAt(\DateTime $startingAt): self
    {
        $this->startingAt = $startingAt;
        return $this;
    }

    public function getEndingAt(): \DateTime
    {
        return $this->endingAt;
    }

    public function setEndingAt(\DateTime $endingAt): self
    {
        $this->endingAt = $endingAt;
        return $this;
    }
}