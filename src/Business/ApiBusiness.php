<?php

namespace App\Business;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Service\Attribute\Required;

class ApiBusiness
{
    private HttpClientInterface $client;

    private RouterInterface $router;

    #[Required]
    public function setClient(HttpClientInterface $bookeo): self
    {
        $this->client = $bookeo;
        return $this;
    }

    #[Required]
    public function setRouter(RouterInterface $router): self
    {
        $this->router = $router;
        return $this;
    }

    public function getBookings(
        \DateTime $startingDateTime = null,
        \DateTime $endingDateTime = null,
        \DateTime $lastUpdatedStartDateTime = null,
        \DateTime $lastUpdatedEndTime = null,
        int $productId = null,
        bool $includeCanceled = false,
        bool $expandCustomer = false,
        bool $expandParticipants = false,
        int $itemsPerPage = 50,
        string $pageNavigationToken = null,
        int $pageNumber = 1
    ): array
    {
        $parameters = [
            'startTime' => $startingDateTime?->format(DATE_ATOM),
            'endTime' => $endingDateTime?->format(DATE_ATOM),
            'lastUpdatedStartTime' => $lastUpdatedStartDateTime?->format(DATE_ATOM),
            'lastUpdatedEndTime' => $lastUpdatedEndTime?->format(DATE_ATOM),
            'productId' => $productId,
            'includeCanceled' => $includeCanceled,
            'expandCustomer' => $expandCustomer,
            'expandParticipants' => $expandParticipants,
            'itemsPerPage' => $itemsPerPage,
            'pageNavigationToken' => $pageNavigationToken,
            'pageNumber' => $pageNumber,
        ];

        return $this->request('bookeo_get_bookings', $parameters);
    }


    protected function request(string $routeName, array $routeParameters = [], string $method = Request::METHOD_GET, array $options = []): array
    {
        try {
            return [
                'result' => true,
                'response' => $this->client->request($method, $this->router->generate($routeName, $routeParameters), $options)->toArray(),
            ];
        } catch (
        \Exception|
        ClientExceptionInterface|
        DecodingExceptionInterface|
        RedirectionExceptionInterface|
        ServerExceptionInterface|
        TransportExceptionInterface
        $exception) {
            return [
                'result' => false,
                'errors' => [$exception->getMessage()],
            ];
        }
    }
}