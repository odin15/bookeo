<?php

namespace App\Business;

use App\Api\AbstractApiRequest;
use Symfony\Contracts\Service\Attribute\Required;

class ApiRequestBusiness
{
    private ApiBusiness $apiBusiness;

    #[Required]
    public function setApiBusiness(ApiBusiness $apiBusiness): self
    {
        $this->apiBusiness = $apiBusiness;

        return $this;
    }

    public function executeRequest(AbstractApiRequest $request): void
    {
        $reflectionClass = new \ReflectionClass($this->apiBusiness);
        $reflectionClass->getMethod()
    }
}