<?php

namespace App\Api\Booking;

use App\Api\AbstractApiRequest;

class GetBookingsRequest extends AbstractApiRequest
{
    private \DateTime $startingTime;

    private \DateTime $endingTime;

    private \DateTime $lastUpdateStartTime;

    private \DateTime $lastUpdateEndTime;

    private int $productId;

    private bool $includeCanceled = false;

    private bool $expandCustomer = false;

    private bool $expandParticipants = false;

    public function getRoute(): string
    {
        return 'bookeo_get_bookings';
    }

    public function getRouteParameter(): array
    {
        return [];
    }
}