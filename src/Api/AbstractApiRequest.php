<?php

namespace App\Api;

abstract class AbstractApiRequest implements ApiRequestInterface
{
    protected int $itemsPerPages = 50;

    protected string $pageNavigationToken;

    protected int $pageNumber = 1;

    public abstract function getRoute(): string;

    public abstract function getMethod(): string;

    public function getRouteParameter(): array
    {
        return [];
    }

    public function getItemsPerPages(): int
    {
        return $this->itemsPerPages;
    }

    public function setItemsPerPages(int $itemsPerPages): self
    {
        $this->itemsPerPages = $itemsPerPages;
        return $this;
    }

    public function getPageNavigationToken(): string
    {
        return $this->pageNavigationToken;
    }

    public function setPageNavigationToken(string $pageNavigationToken): self
    {
        $this->pageNavigationToken = $pageNavigationToken;
        return $this;
    }

    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function setPageNumber(int $pageNumber): self
    {
        $this->pageNumber = $pageNumber;
        return $this;
    }
}