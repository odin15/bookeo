<?php

namespace App\Api;

interface ApiRequestInterface
{
    public function getParameter(string $key): mixed;

    public function getRoute(): string;

    public function getRouteParameter(): array;
}