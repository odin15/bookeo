<?php

namespace App\Command\Event;

use App\Business\ApiBusiness;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(name: 'bookeo:events:all')]
class FetchAllEventsCommand extends Command
{
    private ApiBusiness $bookeoBusiness;

    #[Required]
    public function setBookeoBusiness(ApiBusiness $bookeoBusiness): void
    {
        $this->bookeoBusiness = $bookeoBusiness;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->bookeoBusiness->getBookings(new \DateTime('2022-05-01 00:00:00'), new \DateTime('2022-05-31 23:59:59'));
        return Command::SUCCESS;
    }
}