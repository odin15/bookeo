<?php

namespace App\Model;

class Participant
{
    private string $personId;

    private string $peopleCategoryId;

    private int $categoryIndex;

    private LinkedPerson $personDetails;

    public function getPersonId(): string
    {
        return $this->personId;
    }

    public function setPersonId(string $personId): self
    {
        $this->personId = $personId;
        return $this;
    }

    public function getPeopleCategoryId(): string
    {
        return $this->peopleCategoryId;
    }

    public function setPeopleCategoryId(string $peopleCategoryId): self
    {
        $this->peopleCategoryId = $peopleCategoryId;
        return $this;
    }

    public function getCategoryIndex(): int
    {
        return $this->categoryIndex;
    }

    public function setCategoryIndex(int $categoryIndex): self
    {
        $this->categoryIndex = $categoryIndex;
        return $this;
    }

    public function getPersonDetails(): LinkedPerson
    {
        return $this->personDetails;
    }

    public function setPersonDetails(LinkedPerson $personDetails): self
    {
        $this->personDetails = $personDetails;
        return $this;
    }

}