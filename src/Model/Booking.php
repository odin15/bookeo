<?php

namespace App\Model;

class Booking
{
    private string $bookingNumber;

    private string $eventId;

    private string $firstCourseEnrolledEventId;

    private string $dropinCourseEnrolledEventId;

    private \DateTime $startTime;

    private \DateTime $endTime;

    private string $customerId;

    private Customer $customer;

    private string $title;

    private string $externalRef;

    private array $participants = [];

    private array $resources = [];

    private bool $canceled = false;

    private \DateTime $cancelationTime;

    private string $cancelationAgent;

    private bool $accepted = false;

    private string $sourceIp;

    private \DateTime $creationTime;

    private string $creationAgent;

    private \DateTime $lastChangeTime;

    private string $lastChangeAgent;

    private string $productName;

    private string $productId;

    private Price $price;

    private array $options;

    private bool $privateEvent;

    private string $promotionCodeInput;

    private string $promotionName;

    private string $couponCodes;

    private string $giftVoucherCodeInput;

    private string $specificVoucherCode;

    private array $initialPayments;

    private bool $noShow;

    public function getBookingNumber(): string
    {
        return $this->bookingNumber;
    }

    public function setBookingNumber(string $bookingNumber): self
    {
        $this->bookingNumber = $bookingNumber;
        return $this;
    }

    public function getEventId(): string
    {
        return $this->eventId;
    }

    public function setEventId(string $eventId): self
    {
        $this->eventId = $eventId;
        return $this;
    }

    public function getFirstCourseEnrolledEventId(): string
    {
        return $this->firstCourseEnrolledEventId;
    }

    public function setFirstCourseEnrolledEventId(string $firstCourseEnrolledEventId): self
    {
        $this->firstCourseEnrolledEventId = $firstCourseEnrolledEventId;
        return $this;
    }

    public function getDropinCourseEnrolledEventId(): string
    {
        return $this->dropinCourseEnrolledEventId;
    }

    public function setDropinCourseEnrolledEventId(string $dropinCourseEnrolledEventId): self
    {
        $this->dropinCourseEnrolledEventId = $dropinCourseEnrolledEventId;
        return $this;
    }

    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTime $startTime): self
    {
        $this->startTime = $startTime;
        return $this;
    }

    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTime $endTime): self
    {
        $this->endTime = $endTime;
        return $this;
    }

    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    public function setCustomerId(string $customerId): self
    {
        $this->customerId = $customerId;
        return $this;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getExternalRef(): string
    {
        return $this->externalRef;
    }

    public function setExternalRef(string $externalRef): self
    {
        $this->externalRef = $externalRef;
        return $this;
    }

    public function getParticipants(): array
    {
        return $this->participants;
    }

    public function setParticipants(array $participants): self
    {
        $this->participants = $participants;
        return $this;
    }

    public function getResources(): array
    {
        return $this->resources;
    }

    public function setResources(array $resources): self
    {
        $this->resources = $resources;
        return $this;
    }

    public function isCanceled(): bool
    {
        return $this->canceled;
    }

    public function setCanceled(bool $canceled): self
    {
        $this->canceled = $canceled;
        return $this;
    }

    public function getCancelationTime(): \DateTime
    {
        return $this->cancelationTime;
    }

    public function setCancelationTime(\DateTime $cancelationTime): self
    {
        $this->cancelationTime = $cancelationTime;
        return $this;
    }

    public function getCancelationAgent(): string
    {
        return $this->cancelationAgent;
    }

    public function setCancelationAgent(string $cancelationAgent): self
    {
        $this->cancelationAgent = $cancelationAgent;
        return $this;
    }

    public function isAccepted(): bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;
        return $this;
    }

    public function getSourceIp(): string
    {
        return $this->sourceIp;
    }

    public function setSourceIp(string $sourceIp): self
    {
        $this->sourceIp = $sourceIp;
        return $this;
    }

    public function getCreationTime(): \DateTime
    {
        return $this->creationTime;
    }

    public function setCreationTime(\DateTime $creationTime): self
    {
        $this->creationTime = $creationTime;
        return $this;
    }

    public function getCreationAgent(): string
    {
        return $this->creationAgent;
    }

    public function setCreationAgent(string $creationAgent): self
    {
        $this->creationAgent = $creationAgent;
        return $this;
    }

    public function getLastChangeTime(): \DateTime
    {
        return $this->lastChangeTime;
    }

    public function setLastChangeTime(\DateTime $lastChangeTime): self
    {
        $this->lastChangeTime = $lastChangeTime;
        return $this;
    }

    public function getLastChangeAgent(): string
    {
        return $this->lastChangeAgent;
    }

    public function setLastChangeAgent(string $lastChangeAgent): self
    {
        $this->lastChangeAgent = $lastChangeAgent;
        return $this;
    }

    public function getProductName(): string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): self
    {
        $this->productName = $productName;
        return $this;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): self
    {
        $this->productId = $productId;
        return $this;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;
        return $this;
    }

    public function isPrivateEvent(): bool
    {
        return $this->privateEvent;
    }

    public function setPrivateEvent(bool $privateEvent): self
    {
        $this->privateEvent = $privateEvent;
        return $this;
    }

    public function getPromotionCodeInput(): string
    {
        return $this->promotionCodeInput;
    }

    public function setPromotionCodeInput(string $promotionCodeInput): self
    {
        $this->promotionCodeInput = $promotionCodeInput;
        return $this;
    }

    public function getPromotionName(): string
    {
        return $this->promotionName;
    }

    public function setPromotionName(string $promotionName): self
    {
        $this->promotionName = $promotionName;
        return $this;
    }

    public function getCouponCodes(): string
    {
        return $this->couponCodes;
    }

    public function setCouponCodes(string $couponCodes): self
    {
        $this->couponCodes = $couponCodes;
        return $this;
    }

    public function getGiftVoucherCodeInput(): string
    {
        return $this->giftVoucherCodeInput;
    }

    public function setGiftVoucherCodeInput(string $giftVoucherCodeInput): self
    {
        $this->giftVoucherCodeInput = $giftVoucherCodeInput;
        return $this;
    }

    public function getSpecificVoucherCode(): string
    {
        return $this->specificVoucherCode;
    }

    public function setSpecificVoucherCode(string $specificVoucherCode): self
    {
        $this->specificVoucherCode = $specificVoucherCode;
        return $this;
    }

    public function getInitialPayments(): array
    {
        return $this->initialPayments;
    }

    public function setInitialPayments(array $initialPayments): self
    {
        $this->initialPayments = $initialPayments;
        return $this;
    }

    public function isNoShow(): bool
    {
        return $this->noShow;
    }

    public function setNoShow(bool $noShow): self
    {
        $this->noShow = $noShow;
        return $this;
    }
}