<?php

namespace App\Model;

class LinkedPerson
{
    private string $id;

    private string $firstName;

    private string $middleName;

    private string $lastName;

    private string $emailAddress;

    private array $phoneNumbers;

    private StreetAddress $streetAddress;

    private \DateTime $creationTime;

    private \DateTime $startTimeOfNextBooking;

    private \DateTime $startTimeOfPreviousBooking;

    private \DateTime $dateOfBirth;

    private array $customFields;

    private string $gender;

    private string $customerId;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    public function setEmailAddress(string $emailAddress): self
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }

    public function getPhoneNumbers(): array
    {
        return $this->phoneNumbers;
    }

    public function setPhoneNumbers(array $phoneNumbers): self
    {
        $this->phoneNumbers = $phoneNumbers;
        return $this;
    }

    public function getStreetAddress(): StreetAddress
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(StreetAddress $streetAddress): self
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    public function getCreationTime(): \DateTime
    {
        return $this->creationTime;
    }

    public function setCreationTime(\DateTime $creationTime): self
    {
        $this->creationTime = $creationTime;
        return $this;
    }

    public function getStartTimeOfNextBooking(): \DateTime
    {
        return $this->startTimeOfNextBooking;
    }

    public function setStartTimeOfNextBooking(\DateTime $startTimeOfNextBooking): self
    {
        $this->startTimeOfNextBooking = $startTimeOfNextBooking;
        return $this;
    }

    public function getStartTimeOfPreviousBooking(): \DateTime
    {
        return $this->startTimeOfPreviousBooking;
    }

    public function setStartTimeOfPreviousBooking(\DateTime $startTimeOfPreviousBooking): self
    {
        $this->startTimeOfPreviousBooking = $startTimeOfPreviousBooking;
        return $this;
    }

    public function getDateOfBirth(): \DateTime
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(\DateTime $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;
        return $this;
    }

    public function getCustomFields(): array
    {
        return $this->customFields;
    }

    public function setCustomFields(array $customFields): self
    {
        $this->customFields = $customFields;
        return $this;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    public function setCustomerId(string $customerId): self
    {
        $this->customerId = $customerId;
        return $this;
    }
}