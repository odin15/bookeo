<?php

namespace App\Model;

use DateTime;

class Customer
{
    private string $id;
    
    private string $firstName;
    
    private string $middleName;
    
    private string $lastName;
    
    private string $emailAddress;
    
    private array $phoneNumbers;
    
    private StreetAddress $streetAddress;
    
    private DateTime $creationTime;
    
    private DateTime $startTimeOfNextBooking;
    
    private DateTime $startTimeOfPreviousBooking;
    
    private DateTime $dateOfBirth;
    
    private array $customFields;
    
    private string $gender;
    
    private string $facebookId;
    
    private Money $credit;
    
    private string $languageCode;
    
    private bool $acceptSmsReminders;
    
    private int $numBookings;
    
    private int $numCancelations;
    
    private int $numNoShows;
    
    private int $member;
    
    private DateTime $membershipEnd;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function setMiddleName(string $middleName): self
    {
        $this->middleName = $middleName;
        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    public function setEmailAddress(string $emailAddress): self
    {
        $this->emailAddress = $emailAddress;
        return $this;
    }

    public function getPhoneNumbers(): array
    {
        return $this->phoneNumbers;
    }

    public function setPhoneNumbers(array $phoneNumbers): self
    {
        $this->phoneNumbers = $phoneNumbers;
        return $this;
    }

    public function getStreetAddress(): StreetAddress
    {
        return $this->streetAddress;
    }

    public function setStreetAddress(StreetAddress $streetAddress): self
    {
        $this->streetAddress = $streetAddress;
        return $this;
    }

    public function getCreationTime(): DateTime
    {
        return $this->creationTime;
    }

    public function setCreationTime(DateTime $creationTime): self
    {
        $this->creationTime = $creationTime;
        return $this;
    }

    public function getStartTimeOfNextBooking(): DateTime
    {
        return $this->startTimeOfNextBooking;
    }

    public function setStartTimeOfNextBooking(DateTime $startTimeOfNextBooking): self
    {
        $this->startTimeOfNextBooking = $startTimeOfNextBooking;
        return $this;
    }

    public function getStartTimeOfPreviousBooking(): DateTime
    {
        return $this->startTimeOfPreviousBooking;
    }

    public function setStartTimeOfPreviousBooking(DateTime $startTimeOfPreviousBooking): self
    {
        $this->startTimeOfPreviousBooking = $startTimeOfPreviousBooking;
        return $this;
    }

    public function getDateOfBirth(): DateTime
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(DateTime $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;
        return $this;
    }

    public function getCustomFields(): array
    {
        return $this->customFields;
    }

    public function setCustomFields(array $customFields): self
    {
        $this->customFields = $customFields;
        return $this;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    public function getFacebookId(): string
    {
        return $this->facebookId;
    }

    public function setFacebookId(string $facebookId): self
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    public function getCredit(): Money
    {
        return $this->credit;
    }

    public function setCredit(Money $credit): self
    {
        $this->credit = $credit;
        return $this;
    }

    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    public function setLanguageCode(string $languageCode): self
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    public function isAcceptSmsReminders(): bool
    {
        return $this->acceptSmsReminders;
    }

    public function setAcceptSmsReminders(bool $acceptSmsReminders): self
    {
        $this->acceptSmsReminders = $acceptSmsReminders;
        return $this;
    }

    public function getNumBookings(): int
    {
        return $this->numBookings;
    }

    public function setNumBookings(int $numBookings): self
    {
        $this->numBookings = $numBookings;
        return $this;
    }

    public function getNumCancelations(): int
    {
        return $this->numCancelations;
    }

    public function setNumCancelations(int $numCancelations): self
    {
        $this->numCancelations = $numCancelations;
        return $this;
    }

    public function getNumNoShows(): int
    {
        return $this->numNoShows;
    }

    public function setNumNoShows(int $numNoShows): self
    {
        $this->numNoShows = $numNoShows;
        return $this;
    }

    public function getMember(): int
    {
        return $this->member;
    }

    public function setMember(int $member): self
    {
        $this->member = $member;
        return $this;
    }

    public function getMembershipEnd(): DateTime
    {
        return $this->membershipEnd;
    }

    public function setMembershipEnd(DateTime $membershipEnd): self
    {
        $this->membershipEnd = $membershipEnd;
        return $this;
    }
}