<?php

namespace App\Model;

class Price
{
    private Money $totalGross;

    private Money $totalNet;

    private Money $totalTaxes;

    private Money $totalPaid;

    private array $taxes;

    public function getTotalGross(): Money
    {
        return $this->totalGross;
    }

    public function setTotalGross(Money $totalGross): self
    {
        $this->totalGross = $totalGross;
        return $this;
    }

    public function getTotalNet(): Money
    {
        return $this->totalNet;
    }

    public function setTotalNet(Money $totalNet): self
    {
        $this->totalNet = $totalNet;
        return $this;
    }

    public function getTotalTaxes(): Money
    {
        return $this->totalTaxes;
    }

    public function setTotalTaxes(Money $totalTaxes): self
    {
        $this->totalTaxes = $totalTaxes;
        return $this;
    }

    public function getTotalPaid(): Money
    {
        return $this->totalPaid;
    }

    public function setTotalPaid(Money $totalPaid): self
    {
        $this->totalPaid = $totalPaid;
        return $this;
    }

    public function getTaxes(): array
    {
        return $this->taxes;
    }

    public function setTaxes(array $taxes): self
    {
        $this->taxes = $taxes;
        return $this;
    }
}