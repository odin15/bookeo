<?php

namespace App\Controller\Webhook;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/event')]
class EventController extends AbstractController
{
    #[Route('/create', name: 'event_create')]
    public function createEvent(Request $request, ManagerRegistry $registry): Response
    {

    }

    #[Route('/update', name: 'event_update')]
    public function updateEvent(Request $request): Response
    {

    }

    #[Route('/delete', name: 'event_delete')]
    public function deleteEvent(Request $request): Response
    {

    }
}